Sentel Tech provides the highest quality spy products and surveillance gear to ensure that we meet all of our customer’s surveillance needs. Keeping your family and loved ones safe should not require monthly payments or long complicated subscriptions.                                                            

Our goal is to provide an affordable and easy solution to all of our customers so that they can spend their money on other things. We offer a variety of security solutions for home, business, travel, and just about any situation you can think of. Sentel Tech offers the latest in Spy cameras, Audio recorders, GPS trackers and Security cameras. Whether you are new to home security or surveillance or a seasoned professional, chances are we have the gear, tools, and experts to help you get the job done.

Address: 849 Rivanna River Reach, Chesapeake, VA 23320

Phone: +1 800-936-2504
